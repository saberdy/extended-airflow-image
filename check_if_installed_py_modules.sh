#!/bin/bash

LOCAL_FILE_PATH=python_extended/bookscraper-0.1.0-py3-none-any.whl
PACKAGE_BASE_NAME=$(basename ${LOCAL_FILE_PATH})

DELIMITER=-

MODULE_NAME=$( echo ${PACKAGE_BASE_NAME} | cut -d${DELIMITER} -f1 )
echo
echo ">>> checking $MODULE_NAME >>>"
echo
# MODULE_NAME=book

SCRAPER_EXISTS=$(python3 -m pip freeze | grep $MODULE_NAME | cut -d"=" -f1)

if [[ $SCRAPER_EXISTS == $MODULE_NAME ]];
    then
    	echo 'scraper module is installed!'
        echo
    else 
        echo 'scraper module NOT found!'
        echo
fi

