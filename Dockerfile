FROM apache/airflow:2.8.1-python3.8

#  airflow user needs to be none-root to install python modules
# (according to documentations).
# This is unlike apt package installtions which needs root user.
USER airflow

WORKDIR /opt/airflow/python_extended

ENV LOCAL_WHL_FILE_PATH=python_extended/bookscraper-0.1.0-py3-none-any.whl

# copy necessary files over container 
COPY  ${LOCAL_WHL_FILE_PATH} .
COPY requirements .

# install packages from:
# 1- .whl local file 
# 2- pypi module 
RUN python -m pip install --upgrade --no-cache-dir pip
RUN python -m pip install --no-cache-dir bookscraper-0.1.0-py3-none-any.whl
RUN python -m pip install --no-cache-dir -r requirements

# make sure that modules are installed

COPY check_if_installed_py_modules.sh .
RUN /bin/bash check_if_installed_py_modules.sh
