import os
from airflow.utils.dates import days_ago
from datetime import timedelta

from airflow.decorators import dag, task
from airflow.operators.empty import EmptyOperator
from airflow.providers.amazon.aws.hooks.s3 import S3Hook


dag_abs_path = os.path.abspath(__file__)
parent_parent_dag_file = os.path.dirname(os.path.dirname(dag_abs_path))
dag_name = os.path.basename(__file__).replace('.py', '')

default_args = {
    'owner': 'bookscraper',
    'retries': 3,
    'retry_delay': timedelta(minutes=1)
}


@dag(
    dag_id=dag_name,
    start_date=days_ago(1),
    # end_date=datetime(2022, 9, 12),
    schedule='1-10/1 3 * * *',
    # schedule='* * * * *',
    default_args=default_args,
    catchup=False,
    max_active_runs=1
)
def scraper_pipeline():

    start = EmptyOperator(task_id='start')

    end = EmptyOperator(task_id='end')

    @task(task_id="copy_scraped_files_to_s3")
    def copy_scraped_files_to_s3(**context):
        scraped_data_directory = f'{parent_parent_dag_file}/data'
        bucket_name = 'uploads'
        s3_hook = S3Hook('aws_s3_conn')
        s3_hook.get_conn()
        bucket_exists = s3_hook.check_for_bucket(bucket_name)

        if not bucket_exists:
            print(f'bucket: {bucket_name} does not exist!')
            s3_hook.create_bucket(bucket_name)
            print(f'bucket: {bucket_name} is created successfully!')
        else:
            print(f'bucket: {bucket_name} already exists!')

        s3_hook.load_file(
            filename=f'{scraped_data_directory}/clean_data.json',
            bucket_name=bucket_name,
            key=f'{context["ts_nodash"]}.json',
            replace=True
        )
        print(f'uploaded {scraped_data_directory}/clean_data.json successfully!')

    start >> copy_scraped_files_to_s3() >> end


scraper_pipeline = scraper_pipeline()
