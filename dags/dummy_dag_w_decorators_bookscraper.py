import logging
from datetime import datetime
from airflow.utils.dates import days_ago
from rich.console import Console

from airflow.decorators import dag, task
from airflow.operators.bash import BashOperator

rconsole = Console()


@dag(
    'dummy_scraper_dag',
    schedule=None, # '2 */1 * * *',
    # start_date=datetime(2024, 3, 1),
    start_date=days_ago(1),
    catchup=False,
)
def pipeline_flow():
    @task
    def start_tasks():
        rconsole.print('[bold green]tasks started [via console print]... ')
        logging.info('tasks started [via logging]... ')
        return 'tasks started ... [via return]'

    # @task
    # def scraper():
    #     rconsole.print("[bold green]importing scraper module ... ")
    #     try:
    #         from bookscraper.bookscraper.spiders.bookspider import main
    #     except ImportError as e:
    #         rconsole.print("[bold green]Couldn't import the module bookscraper")
    #         raise ImportError(f'Exception: {e}')

    #     else:
    #         logging.info('imported bookscraper successully!')
    #         main()

    scrape_cli_command = BashOperator(
        task_id='run_scraper_task',
        bash_command='cd /opt/airflow/data; python3 -c "from bookscraper.bookscraper.spiders.bookspider import main; main()" ',
    )

    @task
    def complete_tasks():
        rconsole.print('[bold green]all tasks are completed successfully![via console print] ')
        logging.info('all tasks are completed successfully! [via logging]')
        return 'all tasks are completed successfully! [via return]'

    start_tasks() >> scrape_cli_command >> complete_tasks()


pipeline_dag = pipeline_flow()
