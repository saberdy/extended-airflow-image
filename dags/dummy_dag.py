from datetime import datetime
import pytz
from rich.console import Console

from airflow import DAG
from airflow.operators.python import PythonOperator

rconsole = Console()


def dummy_task(
        date: datetime = datetime.utcnow(),
        tz: str = 'UTC'):
    if tz == 'UTC':
        return f'[bold cyan]It was {date.strftime("%A")} on {date.isoformat()}'
    else:
        tz = pytz.timezone(tz)
        date = datetime.now().astimezone(tz)
        return 'It was {date.strftime("%A")} on {date.isoformat()}'


def complete_tasks():
    rconsole.print('[bold cyan]print all tasks are completed successfully!')
    return 'return all tasks are completed successfully!'


def start_tasks():
    rconsole.print('[bold cyan]print tasks started ... ')
    return 'tasks started ... '


with DAG(
    dag_id='dummy_dag',
    start_date=datetime(2023, 2, 6),
    # schedule_interval=None,
    schedule='*/5 * * * *',
    catchup=False,
) as dummy_dag:
    do_dummy_task = PythonOperator(
        task_id='dummy_task',
        python_callable=dummy_task,
    )
    start = PythonOperator(
        task_id='start',
        python_callable=start_tasks
    )
    end = PythonOperator(
        task_id='end',
        python_callable=complete_tasks
    )

start >> do_dummy_task >> end
