import logging
from datetime import datetime
import pytz
from rich.console import Console

from airflow.decorators import dag, task

rconsole = Console()


@dag(
    'dummy_dag_w_decorators',
    schedule='*/5 * * * *',
    start_date=datetime(2024, 2, 16),
    catchup=False,
)
def pipeline_flow():
    @task
    def start_tasks():
        rconsole.print('[bold cyan]print tasks started [via console print]... ')
        logging.info('tasks started [via logging]... ')
        return 'tasks started ... [via return]'

    @task
    def dummy_task(
            date: datetime = datetime.utcnow(),
            tz: str = 'UTC'):
        if tz == 'UTC':
            return f'[bold cyan]It was {date.strftime("%A")} on {date.isoformat()}'
        else:
            tz = pytz.timezone(tz)
            date = datetime.now().astimezone(tz)
            return 'It was {date.strftime("%A")} on {date.isoformat()}'

    @task
    def complete_tasks():
        rconsole.print('[bold cyan]print all tasks are completed successfully![via console print] ')
        logging.info('all tasks are completed successfully! [via logging]')
        return 'all tasks are completed successfully! [via return]'

    start_tasks()
    dummy_task(datetime.now(), 'Europe/Stockholm')
    complete_tasks()


pipeline_flow()
